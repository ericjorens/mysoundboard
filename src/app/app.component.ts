import { AfterViewInit, Component, QueryList, ViewChildren } from '@angular/core';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { SoundPlayerSoloComponent } from './components/sound-player-solo/sound-player-solo.component';
import { AudioService } from './services/audio.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit{
  title = 'MySoundboard';
  @ViewChildren(SoundPlayerSoloComponent) audioComponents!: QueryList<SoundPlayerSoloComponent>;

  constructor(private audio:AudioService){

  }

  ngAfterViewInit(): void {
      this.audioComponents.changes.subscribe((audio)=>{
        //do nothing
      });
  }

  toggleMute(){
    console.log(this.audioComponents);
    this.audio.muteAudio = !this.audio.muteAudio;
    this.audioComponents.forEach((c:SoundPlayerSoloComponent)=>{
      c.toggleMute();
    });
  }
}
