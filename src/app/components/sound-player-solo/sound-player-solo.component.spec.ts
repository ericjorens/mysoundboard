import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoundPlayerSoloComponent } from './sound-player-solo.component';

describe('SoundPlayerSoloComponent', () => {
  let component: SoundPlayerSoloComponent;
  let fixture: ComponentFixture<SoundPlayerSoloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoundPlayerSoloComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoundPlayerSoloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
