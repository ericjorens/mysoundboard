import { Component, Input, OnInit } from '@angular/core';
import { AudioService } from 'src/app/services/audio.service';

@Component({
  selector: 'app-sound-player-solo',
  templateUrl: './sound-player-solo.component.html',
  styleUrls: ['./sound-player-solo.component.css'],
  providers:[AudioService]
})
export class SoundPlayerSoloComponent implements OnInit {
  @Input() filename:string = '';

  constructor(private audio: AudioService) { }

  ngOnInit(): void {
  }

  clickMe(){
    this.audio.playFile(this.filename);
  }

  toggleMute(){
    this.audio.muteAudio = !this.audio.muteAudio;
  }

}
