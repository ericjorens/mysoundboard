import { Component, Input, OnInit } from '@angular/core';
import { AudioService } from 'src/app/services/audio.service';

@Component({
  selector: 'app-sound-player',
  templateUrl: './sound-player.component.html',
  styleUrls: ['./sound-player.component.css']
})
export class SoundPlayerComponent implements OnInit {

  @Input() filename:string = '';

  constructor(private audio: AudioService) { }

  ngOnInit(): void {

  }

  clickMe(){
    this.audio.playFile(this.filename);
  }

}
