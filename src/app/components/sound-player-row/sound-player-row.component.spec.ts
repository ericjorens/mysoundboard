import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoundPlayerRowComponent } from './sound-player-row.component';

describe('SoundPlayerRowComponent', () => {
  let component: SoundPlayerRowComponent;
  let fixture: ComponentFixture<SoundPlayerRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoundPlayerRowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoundPlayerRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
