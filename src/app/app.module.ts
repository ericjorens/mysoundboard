import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { SoundPlayerComponent } from './components/sound-player/sound-player.component';
import { SoundPlayerRowComponent } from './components/sound-player-row/sound-player-row.component';
import { AudioService } from './services/audio.service';
import { SoundPlayerSoloComponent } from './components/sound-player-solo/sound-player-solo.component';

@NgModule({
  declarations: [
    AppComponent,
    SoundPlayerComponent,
    SoundPlayerRowComponent,
    SoundPlayerSoloComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [AudioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
