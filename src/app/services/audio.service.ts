import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AudioService {
  private readonly BASE_ASSET_PATH = 'assets/';
  private _audio: string[] = [
    'alert.wav',
    'correct.mp3',
    'game-over.wav',
    'wrong.wav',
  ];
  private _audioFile: any[] = [];
  muteAudio = false;

  constructor() {
    this.loadCache();
  }

  /**
   * We want to cache all of the audio files when we init the service
   */
  loadCache() {
    this._audio.forEach((name) => {
      console.log("loading audio " + name);
      let audio = new Audio();
      audio.src = this.BASE_ASSET_PATH + name;
      audio.load();
      this._audioFile.push({
        name: name,
        file: audio,
      });
    });
  }

  /**
   * Play a chached audio file
   * @param filename
   * @param loop
   */
  playFile(filename:string, loop?: boolean){
    this._audioFile.forEach((item)=>{
      if(item.name === filename && !this.muteAudio){
        item.file.loop = loop || false;
        item.file.play();
      }
    });
  }

  /**
   * Play a new audio resource
   * @param filename
   * @param loop
   */
  playAudio(filename: string, loop?: boolean) {
    if (!this.muteAudio) {
      let audio = new Audio();
      audio.src = this.BASE_ASSET_PATH + filename;
      audio.load();
      audio.loop = loop || false;
      audio.play();
    }
  }
}
